import React from 'react'

export default props => (
    <footer className='main-footer'>
        <strong>
            Copyright &copy; 2018
            <a href='https://dcwebsolutions.com.br' target='_blank'> DC WebSolutions</a>.
        </strong>
    </footer>
)
